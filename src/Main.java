import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        ArrayList<String> animals = new ArrayList<>();
        animals.add("kangaroo");
        animals.add("panda");
        animals.add("bear");
        animals.add("penguin");
        animals.add("eagle");

        System.out.println("Animals in the Zoo: " + animals);

        String[] newAnimalsArr = new String[5];
        newAnimalsArr[0] = "kangaroo";
        newAnimalsArr[1] = "panda";
        newAnimalsArr[2] = "bear";
        newAnimalsArr[3] = "penguin";
        newAnimalsArr[4] = "eagle";

        Arrays.sort(newAnimalsArr);
        System.out.println("Animals Alphabetical Order: " + Arrays.toString(newAnimalsArr));

        ArrayList<String> avengers = new ArrayList<>();
        avengers.add("IronMan");
        avengers.add("Hulk");
        avengers.add("Thor");
        avengers.add("Captain America");
        avengers.add("Hawkeye");

        System.out.println("The 5 avengers are: " + avengers);

        HashMap<String,Integer> sabon = new HashMap<>();

        sabon.put("Conditioner",11);
        sabon.put("Shampoo",25);
        sabon.put("Soap",10);

        System.out.println("Our current inventory consists of:");
        System.out.println(sabon);

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Add an Item Name");
        String itemName = numberScanner.nextLine();

        System.out.println("Add Item Stock");
        int itemStock = numberScanner.nextInt();

        System.out.println("Our current inventory consists of:");
        
        sabon.put(itemName, itemStock);
        System.out.println(sabon);













    }
}